module gitlab.com/harikrishnansr92/avs-app

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.13.0
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/gobuffalo/envy v1.10.2 // indirect
	github.com/gobuffalo/fizz v1.14.4 // indirect
	github.com/gobuffalo/flect v1.0.2 // indirect
	github.com/gobuffalo/genny v0.6.0 // indirect
	github.com/gobuffalo/logger v1.0.7
	github.com/gobuffalo/nulls v0.4.2
	github.com/gobuffalo/packd v1.0.2 // indirect
	github.com/gobuffalo/packr/v2 v2.8.0 // indirect
	github.com/gobuffalo/plush/v4 v4.1.18 // indirect
	github.com/gobuffalo/pop v4.13.1+incompatible
	github.com/gobuffalo/validate v2.0.4+incompatible
	github.com/gofrs/uuid v4.3.1+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.12.2
	github.com/graphql-go/graphql v0.8.1
	github.com/graphql-go/handler v0.2.3
	github.com/jackc/pgconn v1.13.0 // indirect
	github.com/karrick/godirwalk v1.16.1 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lib/pq v1.10.7
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/microcosm-cc/bluemonday v1.0.26 // indirect
	github.com/stretchr/testify v1.8.1
	golang.org/x/crypto v0.14.0
)
