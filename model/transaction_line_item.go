package model

import (
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// TransactionLineItem model struct
type TransactionLineItem struct {
	ID                    int64               `db:"id" json:"id"`
	ProductID             int64               `db:"product_id" json:"product_id"`
	Product               Product             `json:"product" belongs_to:"product"`
	UnitPrice             nulls.Float64       `db:"unit_price" json:"unit_price"`
	Quantity              float64             `db:"quantity" json:"quantity"`
	DiscountPercentage    nulls.Float64       `db:"discount_percentage" json:"discount_percentage"`
	TaxID                 nulls.Int64         `db:"tax_id" json:"tax_id"`
	Tax                   Tax                 `json:"tax" belongs_to:"taxes"`
	TransactionDocumentID int64               `db:"transaction_document_id" json:"transaction_document_id"`
	TransactionDocument   TransactionDocument `json:"transaction_document" belongs_to:"transaction_document"`
	LineTotal             float64             `db:"line_total" json:"line_total"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// TransactionLineItems slice of TransactionLineItem
type TransactionLineItems []TransactionLineItem

// Validate presece of Quantity and Product
func (item *TransactionLineItem) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&PresenceValidator{Field: "Quantity", Value: item.Quantity},
		&validators.FuncValidator{
			Field:   "Product",
			Name:    "Product",
			Message: "%s does not exist",
			Fn: func() bool {
				var b bool
				q := tx.Where("id = ?", item.ProductID)
				b, err := q.Exists(&item.Product)
				if err != nil {
					return false
				}
				return b
			},
		},
	), nil
}

// BeforeCreate calculate LineTotal
func (item *TransactionLineItem) BeforeCreate(tx *pop.Connection) error {
	item.setLineTotal(tx)
	return nil
}

// AfterCreate stock update
func (item *TransactionLineItem) AfterCreate(tx *pop.Connection) error {
	doc := TransactionDocument{}
	err := tx.Find(&doc, item.TransactionDocumentID)
	if err != nil {
		return err
	}
	stock := Stock{
		ProductID: item.ProductID,
		ShopID:    doc.ShopID,
		Quantity:  item.Quantity,
	}
	return tx.Create(&stock)
}

func (item *TransactionLineItem) setLineTotal(tx *pop.Connection) {
	tax := Tax{}
	discount := item.DiscountPercentage.Float64
	beforeDiscount := item.Quantity * item.UnitPrice.Float64
	discountAmount := beforeDiscount * (discount / 100)
	afterDiscount := beforeDiscount - discountAmount
	err := tx.Select("taxes.percentage").Find(&tax, item.TaxID.Int64)
	if err != nil {
		item.LineTotal = afterDiscount
		return
	}
	taxAmount := afterDiscount * (tax.Percentage / 100)
	item.LineTotal = afterDiscount + taxAmount
}
