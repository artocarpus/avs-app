package model

import (
	"fmt"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

var docTypes = []string{"Invoice", "Bill"}

// TransactionDocument model struct
type TransactionDocument struct {
	ID                   int64                `db:"id" json:"id"`
	Reference            string               `db:"reference" json:"reference"`
	ContactID            int64                `db:"contact_id" json:"contact_id", rw:"w"`
	Contact              Contact              `json:"contact" belongs_to:"contact"`
	TotalPrice           nulls.Float64        `db:"total_price" json:"total_price"`
	TransactionLineItems TransactionLineItems `json:"transaction_line_items" has_many:"transaction_line_items"`
	ShopID               int64                `db:"shop_id" json:"shop_id"`
	Shop                 Shop                 `json:"shop" belongs_to:"shops"`
	DocType              string               `db:"doc_type" json:"type"`
	IssueDate            string               `db:"issue_date" json:"issue_date"`
	DueDate              string               `db:"due_date" json:"due_date"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// TransactionDocuments slice of TransactionDocument
type TransactionDocuments []TransactionDocument

// Validate IssueDate, DueDate, Reference, Contact and Shop
func (doc *TransactionDocument) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	issueDate, _ := time.Parse("2006-01-05", doc.IssueDate)
	dueDate, _ := time.Parse("2006-01-05", doc.DueDate)
	return validate.Validate(
		&validators.StringIsPresent{
			Name: "Reference", Field: doc.Reference,
			Message: "Reference cannot be empty",
		},
		&validators.TimeAfterTime{
			FirstName:  "DueDate",
			FirstTime:  dueDate,
			SecondName: "IssueDate",
			SecondTime: issueDate,
			Message:    "DueDate must be after IssueDate",
		},
		&validators.FuncValidator{
			Field:   "Reference",
			Name:    "Reference",
			Message: "%s already taken",
			Fn: func() bool {
				var b bool
				q := tx.Where("transaction_documents.reference = ?", doc.Reference)
				b, err = q.Exists(&TransactionDocument{})
				if err != nil {
					return false
				}
				return !b
			},
		},
		&validators.FuncValidator{
			Field:   "Contact",
			Name:    "Contact",
			Message: "%s should be present",
			Fn: func() bool {
				if doc.ContactID == 0 {
					return false
				}
				var b bool
				q := tx.Where("contacts.id = ?", doc.ContactID)
				b, err = q.Exists(&Contact{})
				if err != nil {
					return false
				}
				return b
			},
		}, &validators.FuncValidator{
			Field:   "Shop",
			Name:    "Shop",
			Message: "%s should be present",
			Fn: func() bool {
				if doc.ShopID == 0 {
					return false
				}
				var b bool
				q := tx.Where("shops.id = ?", doc.ShopID)
				b, err = q.Exists(&Shop{})
				if err != nil {
					return false
				}
				return b
			},
		}, &validators.FuncValidator{
			Field:   "DocType",
			Name:    "DocType",
			Message: "%s is not valid",
			Fn: func() bool {
				b := false
				for _, t := range docTypes {
					if t == doc.DocType {
						b = true
					}
				}
				return b
			},
		},
	), nil
}

// BeforeCreate calculate TotalPrice
func (doc *TransactionDocument) BeforeCreate(tx *pop.Connection) error {
	var totalPrice float64
	for _, item := range doc.TransactionLineItems {
		item.setLineTotal(tx)
		totalPrice = totalPrice + item.LineTotal
	}
	doc.TotalPrice = nulls.Float64{totalPrice, true}
	return nil
}

// BeforeDestroy delete line items
func (doc *TransactionDocument) BeforeDestroy(tx *pop.Connection) error {
	lineItems := TransactionLineItems{}
	q := tx.Where(
		"transaction_line_items.transaction_document_id = ?", doc.ID,
	).Select("id")
	err := q.All(&lineItems)
	if err != nil {
		return err
	}
	return tx.Destroy(&lineItems)
}

func (doc *TransactionDocument) createLineItems(tx *pop.Connection) error {
	if doc.ID == 0 {
		return fmt.Errorf("ID not set")
	}
	for _, item := range doc.TransactionLineItems {
		item.TransactionDocumentID = doc.ID
		verrs, err := tx.ValidateAndCreate(&item)
		if verrs.HasAny() {
			return fmt.Errorf("Line items : %s", verrs)
		} else if err != nil {
			return err
		}
	}
	return nil
}

// DocScope Invoice, Bill
func DocScope(docType string) pop.ScopeFunc {
	return func(q *pop.Query) *pop.Query {
		return q.Where("doc_type = ?", docType)
	}
}
