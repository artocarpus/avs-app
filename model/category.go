package model

import (
	"fmt"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// CategoryRepo interface to talk to category table
type CategoryRepo interface {
	Categories(where string) ([]*Category, error)
	Category(id int) (*Category, error)
	CreateCategory(category *Category) error
	UpdateCategory(category *Category) error
	DeleteCategory(id int64) error
}

// Category model struct
type Category struct {
	ID        int64     `db:"id" json:"id"`
	Name      string    `db:"name" json:"name"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// Validate before creating Category
// Use with gobuffalo
func (cat *Category) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{
			Field: cat.Name, Name: "Name",
			Message: "Name must be present",
		},
	), nil
}

// BeforeDestroy checks the category is used by products
func (cat *Category) BeforeDestroy(tx *pop.Connection) error {
	q := tx.Where("products.category_id = ?", cat.ID)
	b, err := q.Exists(&Product{})
	if err != nil {
		return err
	}
	if b {
		var str string
		if cat.Name == "" {
			str = "Category"
		} else {
			str = cat.Name
		}
		fmt.Printf("%s and %d", str, cat.ID)
		return fmt.Errorf("%s is used in products", str)
	}
	return nil
}

//Categories fetch all categories
func (db *DB) Categories() ([]Category, error) {
	categories := []Category{}
	err := db.Conn.All(&categories)
	return categories, err
}

// Category to find a category by id
func (db *DB) Category(id int) (*Category, error) {
	var cat Category
	err := db.Conn.Find(&cat, id)
	return &cat, err
}

// CreateCategory to create a category
func (db *DB) CreateCategory(category *Category) (*validate.Errors, error) {
	return db.Conn.ValidateAndCreate(category)
}

// UpdateCategory updates the category object
func (db *DB) UpdateCategory(category *Category) (*validate.Errors, error) {
	return db.Conn.ValidateAndUpdate(category)
}

// DeleteCategory to destroy a category
func (db *DB) DeleteCategory(id int64) error {
	cat := Category{ID: id}
	return db.Conn.Destroy(&cat)
}
