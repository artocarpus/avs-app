package model

import (
	"fmt"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// Contact model struct
type Contact struct {
	ID        int64        `db:"id" json:"id"`
	Name      string       `db:"name" json:"name"`
	Phone     nulls.String `db:"phone" json:"phone"`
	Email     nulls.String `db:"email" json:"email"`
	AddressID nulls.Int64  `db:"address_id" json:"address_id"`
	Address   Address      `json:"address" belongs_to:"addresses"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// Validate Contact before creating or updating
func (contact *Contact) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{
			Name: "Name", Field: contact.Name,
			Message: "Name cannot be empty",
		},
	), nil
}

// AfterDestroy delete address
func (contact *Contact) AfterDestroy(tx *pop.Connection) error {
	if !contact.AddressID.Valid {
		return nil
	}
	addr := Address{ID: contact.AddressID.Int64}
	return tx.Destroy(&addr)
}

// BeforeDestroy chcek Contact is used in documents
func (contact *Contact) BeforeDestroy(tx *pop.Connection) error {
	q := tx.Where(
		"transaction_documents.contact_id = ?", contact.ID,
	).Select("id")
	b, err := q.Exists(&TransactionDocument{})
	if err != nil {
		return err
	}
	if b {
		return fmt.Errorf("%s is used in document", contact.Name)
	}
	return nil
}
