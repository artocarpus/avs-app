package model

import (
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// Stock defenition for product
type Stock struct {
	ID        int64   `db:"id" json:"id"`
	Quantity  float64 `db:"quantity" json:"quantity"`
	ProductID int64   `db:"product_id" json:"product_id"`
	Product   Product `json:"product" belongs_to:"products"`
	ShopID    int64   `db:"shop_id" json:"shop_id"`
	Shop      Shop    `json:"shop" belongs_to:"shops"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// Stocks slice of stock
type Stocks []Stock

// Validate quantity, product before save
func (stock *Stock) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&PresenceValidator{
			Field: "Quantity", Value: stock.Quantity,
		}, &validators.FuncValidator{
			Field:   "Product",
			Name:    "Product",
			Message: "%s must be present",
			Fn: func() bool {
				if stock.ProductID == 0 {
					return false
				}
				q := tx.Where("products.id = ?", stock.ProductID)
				b, err := q.Exists(&Product{})
				if err != nil {
					return false
				}
				return b
			},
		}, &validators.FuncValidator{
			Field: "Shop", Name: "Shop",
			Message: "%s must be present",
			Fn: func() bool {
				if stock.ShopID == 0 {
					return false
				}
				q := tx.Where("shops.id = ?", stock.ShopID)
				b, err := q.Exists(&Shop{})
				if err != nil || b == false {
					return false
				}
				return true
			},
		},
	), nil
}
