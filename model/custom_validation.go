package model

import (
	"fmt"
	"reflect"
	"strings"

	v "github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// PresenceValidator for checking empty string zero and nil
type PresenceValidator struct {
	Field string
	Value interface{}
}

// GreaterThanOrEqualToZeroValidator check the number is
// Greater than or equal to zero
type GreaterThanOrEqualToZeroValidator struct {
	Name    string
	Field   interface{}
	Message string
}

// IsValid check valid for PresenceValidator
func (v *PresenceValidator) IsValid(errors *v.Errors) {
	var valid bool
	rv := reflect.ValueOf(v.Value)
	if v.Value == nil {
		valid = false
	}

	switch rv.Kind() {
	case reflect.String:
		valid = !(v.Value == "")
	case reflect.Int:
		valid = !(v.Value == 0)
	case reflect.Int64:
		valid = !(v.Value == int64(0))
	case reflect.Float64:
		valid = !(v.Value == float64(0))
	}

	if valid == false {
		errors.Add(
			strings.ToLower(v.Field),
			fmt.Sprintf("%s must not be empty", v.Field),
		)
	}
}

// IsValid check valid for GreaterThanOrEqualToZeroValidator
func (v *GreaterThanOrEqualToZeroValidator) IsValid(errors *v.Errors) {
	var valid bool
	rv := reflect.ValueOf(v.Field)
	if v.Field == nil {
		valid = false
	}

	switch rv.Kind() {
	case reflect.Int:
		valid = v.Field.(int) >= 0
	case reflect.Int64:
		valid = v.Field.(int64) >= int64(0)
	case reflect.Float64:
		valid = v.Field.(float64) >= float64(0)
	}

	if valid {
		return
	}

	if len(v.Message) > 0 {
		errors.Add(validators.GenerateKey(v.Name), v.Message)
		return
	}

	errors.Add(
		validators.GenerateKey(v.Name),
		fmt.Sprintf("%d is not greater than or equal to zero.", v.Field),
	)
}
