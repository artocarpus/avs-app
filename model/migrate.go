package model

import (
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file" // load migration files
	_ "github.com/lib/pq"                                // postgres driver
)

// MigrateUp from ./migrate/main.sql
func (db *DB) MigrateUp() error {
	driver, err := postgres.WithInstance(db.sqlDB, &postgres.Config{})
	if err != nil {
		return err
	}
	path := getMigrationFile()
	m, err := migrate.NewWithDatabaseInstance(
		path, "postgres", driver,
	)

	if err != nil {
		return err
	}

	err = m.Up()
	if err != nil {
		return err
	}
	return nil
}

// MigrateDown down from ./migrate/main.sql
func (db *DB) MigrateDown() error {
	driver, err := postgres.WithInstance(db.sqlDB, &postgres.Config{})
	if err != nil {
		return err
	}
	path := getMigrationFile()
	m, err := migrate.NewWithDatabaseInstance(
		path, "postgres", driver,
	)

	if err != nil {
		return err
	}

	err = m.Down()
	if err != nil {
		return err
	}
	return nil
}

// Truncate db
func (db *DB) Truncate() error {
	driver, err := postgres.WithInstance(db.sqlDB, &postgres.Config{})
	if err != nil {
		return err
	}
	path := getMigrationFile()
	m, err := migrate.NewWithDatabaseInstance(
		path, "postgres", driver,
	)

	if err != nil {
		return err
	}

	err = m.Drop()
	if err != nil {
		return err
	}
	return nil
}

func getMigrationFile() string {
	currentDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	path := fmt.Sprintf("file://%s/model/migrations", currentDir)
	return path
}
