// Package model
package model

import (
	"testing"

	"github.com/gobuffalo/pop/nulls"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCategory(t *testing.T) {
	OpenDB("test")
	defer t.Cleanup(func() {
		dbConn.Conn.TruncateAll()
	})
	r := require.New(t)
	cat := Category{Name: "Sweets"}

	t.Run("Validate", func(t1 *testing.T) {
		t1.Run("When name is present", func(t2 *testing.T) {
			r = require.New(t2)
			verrs, err := cat.Validate(dbConn.Conn)
			r.NoError(err)
			if verrs.HasAny() {
				t2.Error("Should not have any validation error")
			}
		})

		t1.Run("When name is not present", func(t2 *testing.T) {
			cat.Name = ""
			verrs, err := cat.Validate(dbConn.Conn)
			r.NoError(err)
			expected := "Name must be present"
			assert.Equal(t2, verrs.Error(), expected)
		})
	})

	t.Run("BeforeDestroy", func(t1 *testing.T) {
		r = require.New(t1)

		err := dbConn.Conn.Create(&cat)
		r.NoError(err)

		t1.Run("When products with category", func(t2 *testing.T) {
			r = require.New(t2)

			err := cat.BeforeDestroy(dbConn.Conn)
			r.NoError(err)
		})

		t1.Run("When products are used with category", func(t2 *testing.T) {
			r = require.New(t2)
			defer t2.Cleanup(func() {
				dbConn.Conn.TruncateAll()
			})

			tax := Tax{
				Name:       "15% VAT",
				Percentage: 10,
			}
			dbConn.Conn.Create(&tax)
			product := Product{
				Name: "Grape Juice", Price: 10,
				Sku: "PR301", SalePrice: nulls.Float64{10, true},
				Description: nulls.String{"Grape", true},
				CategoryID:  cat.ID, TaxID: tax.ID,
			}
			err = dbConn.Conn.Create(&product)
			r.NoError(err)

			err := cat.BeforeDestroy(dbConn.Conn)
			r.Error(err)
		})
	})

	t.Run("Categories", func(t1 *testing.T) {
		r = require.New(t1)
		err := dbConn.Conn.Create(&cat)
		r.NoError(err)
		defer t1.Cleanup(func() {
			dbConn.Conn.TruncateAll()
		})
		cats, err := dbConn.Categories()
		r.NoError(err)

		if len(cats) < 1 {
			t1.Error("No categories")
		}
	})

	t.Run("Category", func(t1 *testing.T) {
		r = require.New(t1)

		defer t1.Cleanup(func() {
			dbConn.Conn.TruncateAll()
		})

		err := dbConn.Conn.Create(&cat)
		r.NoError(err)

		newCat, err := dbConn.Category(int(cat.ID))
		r.NoError(err)
		if newCat.ID != cat.ID {
			t1.Error("Invalid category fetched")
		}
	})

	t.Run("CreateCategory", func(t1 *testing.T) {
		t1.Run("When category is valid", func(t2 *testing.T) {
			r = require.New(t2)
			defer t2.Cleanup(func() {
				dbConn.Conn.TruncateAll()
			})

			cat.Name = "Sweets"
			verrs, err := dbConn.CreateCategory(&cat)
			r.NoError(err)
			if verrs.HasAny() {
				t1.Errorf("Invalid category %s", verrs.Error())
			}
		})

		t1.Run("When category is valid", func(t2 *testing.T) {
			r = require.New(t2)
			defer t2.Cleanup(func() {
				dbConn.Conn.TruncateAll()
			})

			cat.Name = ""
			verrs, err := dbConn.CreateCategory(&cat)
			r.NoError(err)
			if !verrs.HasAny() {
				t1.Errorf("Category should not be created without name")
			}
		})
	})

	t.Run("UpdateCategory", func(t1 *testing.T) {
		t1.Run("When category is valid", func(t2 *testing.T) {
			r = require.New(t2)
			defer t2.Cleanup(func() {
				dbConn.Conn.TruncateAll()
			})

			cat.Name = "Sweet Potatto"
			verrs, err := dbConn.UpdateCategory(&cat)
			r.NoError(err)
			if verrs.HasAny() {
				t1.Errorf("Invalid category %s", verrs.Error())
			}
		})

		t1.Run("When category is valid", func(t2 *testing.T) {
			r = require.New(t2)
			defer t2.Cleanup(func() {
				dbConn.Conn.TruncateAll()
			})

			cat.Name = ""
			verrs, err := dbConn.UpdateCategory(&cat)
			r.NoError(err)
			if !verrs.HasAny() {
				t1.Errorf("Category should not be updated without name")
			}
		})
	})

	t.Run("DeleteCategory", func(t1 *testing.T) {
		r = require.New(t1)
		cat.Name = "Rice"
		dbConn.Conn.Create(&cat)
		defer t1.Cleanup(func() {
			dbConn.Conn.TruncateAll()
		})
		catID := cat.ID
		err := dbConn.DeleteCategory(cat.ID)
		r.NoError(err)

		foundCat := Category{}
		err = dbConn.Conn.Find(&foundCat, catID)
		r.Error(err)
	})
}
