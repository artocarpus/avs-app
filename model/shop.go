package model

import (
	"fmt"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// Shop model struct
type Shop struct {
	ID        int64       `db:"id" json:"id"`
	Name      string      `db:"name" json:"name"`
	AddressID nulls.Int64 `db:"address_id" json:"address_id"`
	Address   Address     `json:"address" belongs_to:"addresses"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// Shops slice of shop
type Shops []Shop

// Validate shop before save
func (shop *Shop) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{
			Field: shop.Name, Name: "Name",
			Message: "Name must be present",
		},
	), nil
}

// BeforeSave save address
func (shop *Shop) BeforeSave(tx *pop.Connection) error {
	_, err := tx.Eager().ValidateAndSave(&shop.Address)
	if err != nil {
		return err
	}
	shop.AddressID = nulls.Int64{shop.Address.ID, true}
	return nil
}

// AfterDestroy delete address
func (shop *Shop) AfterDestroy(tx *pop.Connection) error {
	if !shop.AddressID.Valid {
		return nil
	}
	addr := Address{ID: shop.AddressID.Int64}
	return tx.Destroy(&addr)
}

// BeforeDestroy check shop is used
func (shop *Shop) BeforeDestroy(tx *pop.Connection) error {
	q := tx.Where("transaction_documents.shop_id = ?", shop.ID).Select("id")
	b, err := q.Exists(&TransactionDocument{})
	if err != nil {
		return err
	}
	if b {
		return fmt.Errorf("%s is used, Cannot delete", shop.Name)
	}
	return nil
}
