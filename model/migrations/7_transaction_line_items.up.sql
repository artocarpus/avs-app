CREATE TABLE IF NOT EXISTS transaction_line_items (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    product_id integer NOT NULL REFERENCES products (id),
    unit_price NUMERIC(6, 4),
    quantity NUMERIC(6, 4) NOT NULL,
    discount_percentage NUMERIC(6, 4),
    tax_id integer REFERENCES taxes (id),
    transaction_document_id BIGINT NOT NULL REFERENCES transaction_documents (id),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
