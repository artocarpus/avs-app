ALTER TABLE transaction_line_items
    ADD COLUMN line_total NUMERIC(6, 4) NOT NULL
