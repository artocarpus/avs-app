CREATE TABLE IF NOT EXISTS transaction_documents (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    reference varchar(255) NOT NULL UNIQUE,
    contact_id BIGINT NOT NULL REFERENCES contacts (id),
    total_price NUMERIC(6, 4),
    issue_date DATE NOT NULL DEFAULT CURRENT_DATE,
    due_date DATE NOT NULL DEFAULT CURRENT_DATE,
    description TEXT,
    doc_type varchar(255) NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
