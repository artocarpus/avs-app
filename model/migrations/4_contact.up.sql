CREATE TABLE IF NOT EXISTS contacts (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    phone varchar(255),
    email varchar(255),
    address_id BIGINT REFERENCES addresses (id),

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
