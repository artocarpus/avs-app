CREATE TABLE IF NOT EXISTS addresses (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    building VARCHAR(255),
    street VARCHAR(255),
    city VARCHAR(255),
    pin VARCHAR(255),
    state_code VARCHAR(255),
    country_code VARCHAR(255),

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
