ALTER TABLE transaction_documents 
    ADD COLUMN shop_id BIGINT NOT NULL REFERENCES shops (id);
