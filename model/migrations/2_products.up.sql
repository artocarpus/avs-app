CREATE TABLE IF NOT EXISTS products (
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    sku VARCHAR(255) NOT NULL UNIQUE,
    category_id integer REFERENCES categories (id),
    price NUMERIC(6, 4) NOT NULL,
    sale_price NUMERIC(6, 4) NOT NULL,
    description varchar(255),
    hide bool DEFAULT false,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX products_categories ON products(category_id);
