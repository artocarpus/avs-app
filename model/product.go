package model

import (
	"fmt"
	"log"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// ProductRepo interface to talk to product table
type ProductRepo interface {
	Products(where string) ([]*Product, error)
	Product(id int) (*Product, error)
	CreateProduct(product *Product) error
	UpdateProduct(product *Product) error
	DeleteProduct(id int64) error
}

// Product model struct
type Product struct {
	ID                   int64                `db:"id" json:"id"`
	Name                 string               `db:"name" json:"name"`
	Sku                  string               `db:"sku" json:"sku"`
	CategoryID           int64                `db:"category_id" json:"category_id,string,omitempty"`
	Price                float64              `db:"price" json:"price"`
	SalePrice            nulls.Float64        `db:"sale_price" json:"sale_price"`
	Description          nulls.String         `db:"description" json:"description"`
	Hide                 bool                 `db:"hide" json:"hide"`
	Category             Category             `json:"category" belongs_to:"category"`
	TaxID                int64                `db:"tax_id" json:"tax_id,string,omitempty"`
	Tax                  Tax                  `json:"tax" belongs_to:"taxes"`
	TransactionLineItems TransactionLineItems `has_many:"transaction_line_items"`
	Stocks               Stocks               `has_many:"stocks"`
	CreatedAt            time.Time            `db:"created_at" json:"created_at"`
	UpdatedAt            time.Time            `db:"updated_at" json:"updated_at"`
}

// Validate product before saving
func (prod *Product) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{
			Name: "Sku", Field: prod.Sku,
			Message: "Sku cannot be empty",
		}, &validators.StringIsPresent{
			Name: "Name", Field: prod.Name,
			Message: "Name cannot be empty",
		}, &PresenceValidator{Field: "Price", Value: prod.Price},
		&PresenceValidator{Field: "SalePrice", Value: prod.SalePrice.Float64},
		&validators.FuncValidator{
			Field:   "Sku",
			Name:    "Sku",
			Message: "%s already taken",
			Fn: func() bool {
				var b bool
				q := tx.Where("products.sku = ? AND id != ?", prod.Sku, prod.ID)
				b, err := q.Exists(&Product{})
				if err != nil {
					fmt.Println(err)
					return false
				}
				return !b
			},
		},
		&validators.FuncValidator{
			Field: "Tax", Name: "Tax",
			Message: "%s must be present",
			Fn: func() bool {
				if prod.TaxID == 0 {
					return false
				}
				q := tx.Where("taxes.id = ?", prod.TaxID)
				b, err := q.Exists(&Tax{})
				if err != nil {
					return false
				}
				return b
			},
		}, &validators.FuncValidator{
			Field: "Category", Name: "Category",
			Message: "%s must be present",
			Fn: func() bool {
				if prod.CategoryID == 0 {
					return false
				}
				q := tx.Where("categories.id = ?", prod.CategoryID)
				b, err := q.Exists(&Category{})
				if err != nil {
					return false
				}
				return b
			},
		},
	), nil
}

// BeforeDestroy validations
func (prod *Product) BeforeDestroy(tx *pop.Connection) error {
	q := tx.Where("transaction_line_items.product_id = ?", prod.ID).Select("id")
	b, err := q.Exists(&TransactionLineItem{})
	if err != nil {
		return err
	}
	if b {
		return fmt.Errorf("%s cannot be deleted, Used in line items", prod.Name)
	}
	return nil
}

//Products fetch all products
func (db *DB) Products(where string) ([]Product, error) {
	products := []Product{}
	q := db.Conn.Eager("Category", "Tax").Order("id asc")
	if where != "" {
		q = q.Where(where)
	}
	err := q.All(&products)

	return products, err
}

// Product to find a product by id
func (db *DB) Product(id int) (*Product, error) {
	var prod Product
	err := db.Conn.Eager().Find(&prod, id)

	return &prod, err
}

// CreateProduct to create a product
func (db *DB) CreateProduct(product *Product) (*validate.Errors, error) {
	verrs, err := db.Conn.Eager().ValidateAndCreate(product)
	db.Conn.Load(product, "Category", "Tax")

	return verrs, err
}

// UpdateProduct updates the product object
func (db *DB) UpdateProduct(product *Product) (*validate.Errors, error) {
	verrs, err := db.Conn.ValidateAndUpdate(product)
	if err == nil && !verrs.HasAny() {
		db.Conn.Update(&product.Category)
		db.Conn.ValidateAndUpdate(&product.Tax)
	}
	return verrs, err
}

// DeleteProduct to destroy a product
func (db *DB) DeleteProduct(id int64) error {
	product := Product{ID: id}
	err := db.Conn.Find(&product, id)
	if err != nil {
		return err
	}
	return db.Conn.Destroy(&product)
}

// Quantity return stock available for shop
func (prod *Product) Quantity(shopID int64) float64 {
	stocks := Stocks{}
	q := float64(0)
	if len(prod.Stocks) > 1 && prod.Stocks[0].ID != 0 {
		stocks = prod.Stocks
	} else {
		err := dbConn.Conn.Where(
			"stocks.product_id = ? AND stocks.shop_id = ?",
			prod.ID, shopID,
		).All(&stocks)
		if err != nil {
			log.Println(err.Error())
		}
	}
	for _, st := range stocks {
		q = q + st.Quantity
	}
	return q
}
