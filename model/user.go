package model

import (
	"encoding/base64"
	"errors"
	"time"

	"crypto/rand"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
	"golang.org/x/crypto/bcrypt"
)

// User struct
type User struct {
	ID                  int64        `db:"id" json:"id"`
	FullName            nulls.String `db:"full_name" json:"full_name"`
	Email               string       `db:"email" json:"email"`
	SignInCount         int          `db:"sign_in_count" json:"sign_in_count"`
	Password            string       `json:"password" db:"-"`
	ConfirmPassword     string       `json:"confirm_password" db:"-"`
	EncryptedPassword   string       `db:"encrypted_password" json:"encrypted_password"`
	ResetPasswordToken  nulls.String `db:"reset_password_token" json:"reset_password_token"`
	ResetPasswordSentAt nulls.Time   `db:"reset_password_sent_at" json:"reset_password_sent_at"`
	CurrentSignInIP     nulls.String `db:"current_sign_in_ip" json:"current_sign_in_ip"`
	LastSignInIP        nulls.String `db:"last_sign_in_ip" json:"last_sign_in_ip"`
	LastSignInAt        nulls.Time   `db:"last_sign_in_at" json:"last_sign_in_at"`
	CurrentSignInAt     nulls.Time   `db:"current_sign_in_at" json:"current_sign_in_at"`
	RememberCreatedAt   nulls.Time   `db:"remember_created_at" json:"remember_created_at"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// Validate Email
func (user *User) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.EmailIsPresent{
			Field: user.Email, Name: "Email",
			Message: "Mail is not in the right format.",
		},
		&validators.FuncValidator{
			Field: user.Email, Name: "Email",
			Message: "%s already exists",
			Fn: func() bool {
				q := tx.Where("users.email = ? AND id != ?", user.Email, user.ID)
				b, err := q.Exists(&User{})
				if err != nil || b == true {
					return false
				}
				return true
			},
		},
	), nil
}

// ValidateCreate when creating check Password and ConfirmPassword are same
func (user *User) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	return validate.Validate(
		&validators.StringIsPresent{Field: user.Password, Name: "Password"},
		&validators.StringsMatch{
			Name: "Password", Field: user.Password, Field2: user.ConfirmPassword,
			Message: "Password does not match confirmation"},
	), err
}

// BeforeCreate encrypt Password
func (user *User) BeforeCreate(tx *pop.Connection) error {
	return generatePassword(user)
}

// BeforeUpdate checks password change
func (user *User) BeforeUpdate(tx *pop.Connection) error {
	if user.Password == "" {
		return nil
	}
	return nil
}

// UserRepo to interact with users
type UserRepo interface {
	Users(where string) ([]*User, error)
	CreateUser(user *User) error
	User(id int64) (User, error)
}

// Users return array of User
func (db *DB) Users() ([]User, error) {
	users := []User{}
	err := db.Conn.All(&users)
	return users, err
}

// User finds a user with id
func (db *DB) User(id int64) (User, error) {
	var user User
	err := db.Conn.Find(&user, id)
	return user, err
}

// UserByEmail finds user by email
func (db *DB) UserByEmail(email string, user *User) error {
	return db.Conn.Where("email = ?", email).First(user)
}

// CreateUser creates a user
func (db *DB) CreateUser(user *User) (*validate.Errors, error) {
	verrs, err := db.Conn.ValidateAndCreate(user)
	user.Password = ""
	user.ConfirmPassword = ""
	return verrs, err
}

// UpdateUser update row in users table
func (db *DB) UpdateUser(user *User) (*validate.Errors, error) {
	verrs, err := db.Conn.ValidateAndUpdate(user)
	user.Password = ""
	user.ConfirmPassword = ""
	return verrs, err
}

// ResetPassword generates a new password when vaid restToken is passed
func (db *DB) ResetPassword(user *User) *validate.Errors {
	error := validate.NewErrors()
	if user.Password == "" || user.Password != user.ConfirmPassword {
		error.Add("Password", "Password and confiirm password does not match")
		return error
	}

	err := generatePassword(user)
	if err != nil {
		error.Add("Password", err.Error())
		return error
	}

	user.ResetPasswordToken = nulls.NewString("")
	error, _ = db.UpdateUser(user)
	return error
}

// GenerateResetPasswordToken generates a reset password token
func (db *DB) GenerateResetPasswordToken(user *User) error {
	bt := make([]byte, 24)
	_, err := rand.Read(bt)

	if err != nil {
		return err
	}

	user.ResetPasswordToken = nulls.NewString(base64.URLEncoding.EncodeToString(bt))
	user.ResetPasswordSentAt = nulls.NewTime(time.Now())
	verr, err := db.UpdateUser(user)
	if verr.HasAny() {
		err = errors.New(verr.Error())
		return err
	}

	if err != nil {
		return err
	}

	return nil
}

// ComparePasswords validates passwords
func (user *User) ComparePasswords(password string) error {
	return compareHashedPassword([]byte(user.EncryptedPassword), []byte(password))
}

func compareHashedPassword(encrypted []byte, plain []byte) error {
	return bcrypt.CompareHashAndPassword(encrypted, plain)
}

func generatePassword(user *User) error {
	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.EncryptedPassword = string(pass)
	return nil
}
