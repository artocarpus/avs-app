// Package mode provides ...
package model

import (
	"testing"

	"github.com/gobuffalo/pop/nulls"
	"github.com/stretchr/testify/require"
)

func TestAddress(t *testing.T) {
	OpenDB("test")
	defer t.Cleanup(func() {
		dbConn.Conn.TruncateAll()
	})
	r := require.New(t)

	addr := Address{
		Building:    nulls.String{"22", true},
		Street:      nulls.String{"Baker Street", true},
		City:        nulls.String{"London", true},
		Pin:         nulls.String{"2349849", true},
		StateCode:   nulls.String{"023", true},
		CountryCode: nulls.String{"02", true},
	}

	if addr.ID != 0 {
		t.Error("Invalid ID type")
	}

	err := dbConn.Conn.Create(&addr)
	r.NoError(err)
}
