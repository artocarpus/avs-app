package model

import (
	"fmt"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// Tax model struct
type Tax struct {
	ID         int64   `db:"id" json:"id"`
	Name       string  `db:"name" json:"name"`
	Percentage float64 `db:"percentage" json:"percentage"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// Taxes slice of Tax
type Taxes []Tax

// Validate name and Percentage
func (tax *Tax) Validate(tx *pop.Connection) (*validate.Errors, error) {
	val := tax.Percentage

	return validate.Validate(
		&validators.StringIsPresent{
			Name: "Name", Field: tax.Name,
			Message: "Name must be present",
		},
		&GreaterThanOrEqualToZeroValidator{
			Name: "Percentage", Field: val,
			Message: "Percentage must be greater than 0",
		},
	), nil
}

// BeforeDestroy check used in products or line items
func (tax *Tax) BeforeDestroy(tx *pop.Connection) error {
	if err := tax.checkProducts(tx); err != nil {
		return err
	}
	if err := tax.checkLineItems(tx); err != nil {
		return err
	}
	return nil
}

func (tax *Tax) checkProducts(tx *pop.Connection) error {
	q := tx.Where("products.tax_id = ?", tax.ID).Select("id")
	b, err := q.Exists(&Product{})
	if err != nil {
		return err
	}
	if b {
		var str string
		if tax.Name == "" {
			str = "Tax"
		} else {
			str = tax.Name
		}
		fmt.Printf("%s and %d", str, tax.ID)
		return fmt.Errorf("%s is used in products", str)
	}
	return nil
}

func (tax *Tax) checkLineItems(tx *pop.Connection) error {
	q := tx.Where("transaction_line_items.tax_id = ?", tax.ID).Select("id")
	b, err := q.Exists(&TransactionLineItem{})
	if err != nil {
		return err
	}
	if b {
		var str string
		if tax.Name == "" {
			str = "Tax"
		} else {
			str = tax.Name
		}
		fmt.Printf("%s and %d", str, tax.ID)
		return fmt.Errorf("%s is used in lines", str)
	}
	return nil
}
