package model

import (
	"time"

	"github.com/gobuffalo/pop/nulls"
)

// Address for contact and Shop
type Address struct {
	ID          int64        `db:"id" json:"id"`
	Building    nulls.String `db:"building" json:"building"`
	Street      nulls.String `db:"street" json:"stree"`
	City        nulls.String `db:"city" json:"city"`
	Pin         nulls.String `db:"pin" json:"pin"`
	StateCode   nulls.String `db:"state_code" json:"state_code"`
	CountryCode nulls.String `db:"country_code" json:"country_code"`

	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}
