package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"
)

// NullTime type for sql.NullTime
type NullTime sql.NullTime

// NullString type for sql.NullString
type NullString sql.NullString

// NullFloat64 type for sql.NullFloat64
type NullFloat64 sql.NullFloat64

// NormalDate time.Time to Date
type NormalDate time.Time

// MarshalJSON for NullTime
func (ti NullTime) MarshalJSON() ([]byte, error) {
	if !ti.Valid {
		return []byte("null"), nil
	}
	val := fmt.Sprintf("\"%s\"", ti.Time.Format(time.RFC3339))

	return json.Marshal(val)
}

// UnmarshalJSON for NullTime
func (ti *NullTime) UnmarshalJSON(b []byte) error {
	s := string(b)
	// s = Stripchars(s, "\"")

	x, err := time.Parse(time.RFC3339, s)
	if err != nil {
		ti.Valid = false
		return err
	}

	ti.Time = x
	ti.Valid = true
	return nil
}

// Scan implements the Scanner interface for NullTime
func (ti *NullTime) Scan(value interface{}) error {
	var t sql.NullTime
	if err := t.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ti = NullTime{t.Time, false}
	} else {
		*ti = NullTime{t.Time, true}
	}

	return nil
}

// MarshalJSON for NullString
func (ns *NullString) MarshalJSON() ([]byte, error) {
	if !ns.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ns.String)
}

// UnmarshalJSON for NullString
func (ns *NullString) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &ns.String)
	ns.Valid = (err == nil)
	return err
}

// Scan implements the Scanner interface for NullString
func (ns *NullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ns = NullString{s.String, false}
	} else {
		*ns = NullString{s.String, true}
	}

	return nil
}

// MarshalJSON NullFloat64
func (nf *NullFloat64) MarshalJSON() ([]byte, error) {
	if !nf.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(nf.Float64)
}

// UnmarshalJSON for NullFloat64
func (nf *NullFloat64) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &nf.Float64)
	nf.Valid = (err == nil)
	return err
}

// Scan implements the Scanner interface for NullFloat64
func (nf *NullFloat64) Scan(value interface{}) error {
	var f sql.NullFloat64
	if err := f.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*nf = NullFloat64{f.Float64, false}
	} else {
		*nf = NullFloat64{f.Float64, true}
	}

	return nil
}

// MarshalJSON for NormalDate
func (nd NormalDate) MarshalJSON() ([]byte, error) {
	return json.Marshal(nd)
}

// UnmarshalJSON for NormalDate
func (nd *NormalDate) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")
	t, err := time.Parse("2006-01-02", s)
	if err != nil {
		return err
	}
	*nd = NormalDate(t)
	return nil
}

// Scan NormalDate
func (nd *NormalDate) Scan(value interface{}) error {
	*nd, _ = value.(NormalDate)
	return nil
}
