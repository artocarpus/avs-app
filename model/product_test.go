package model

import (
	"testing"

	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestValidate(t *testing.T) {
	OpenDB("test")
	defer t.Cleanup(func() {
		dbConn.Conn.TruncateAll()
	})
	r := require.New(t)
	tax := Tax{
		Name:       "15% VAT",
		Percentage: 10,
	}
	err := dbConn.Conn.Create(&tax)
	r.NoError(err)

	cat := Category{
		Name: "Sweets",
	}
	err = dbConn.Conn.Create(&cat)
	r.NoError(err)

	t.Run("When sku, category and tax is empty", func(t1 *testing.T) {
		product := Product{
			ID: 1, Name: "Grape", Price: 10,
			SalePrice:   nulls.Float64{10, true},
			Description: nulls.String{"Grape", true},
		}
		wantErrs := validate.NewErrors()
		addWantedErrors(wantErrs)
		gotErr, err := product.Validate(dbConn.Conn)
		r.NoError(err)

		assert.Equal(t1, wantErrs.String(), gotErr.String())
	})

	t.Run("When category id is not valid", func(t1 *testing.T) {
		product := Product{
			ID: 1, Name: "Grape", Price: 10,
			Sku: "PR001", SalePrice: nulls.Float64{10, true},
			Description: nulls.String{"Grape", true},
			CategoryID:  100, TaxID: tax.ID,
		}
		wantErrs := validate.NewErrors()
		wantErrs.Add("category", "Category must be present")
		gotErr, err := product.Validate(dbConn.Conn)
		r.NoError(err)

		assert.Equal(t1, wantErrs.String(), gotErr.String())
	})

	t.Run("When TaxID is not valid", func(t1 *testing.T) {
		product := Product{
			ID: 1, Name: "Grape", Price: 10,
			Sku: "PR001", SalePrice: nulls.Float64{10, true},
			Description: nulls.String{"Grape", true},
			CategoryID:  cat.ID, TaxID: 100,
		}
		wantErrs := validate.NewErrors()
		wantErrs.Add("tax", "Tax must be present")
		gotErr, err := product.Validate(dbConn.Conn)
		r.NoError(err)

		assert.Equal(t1, wantErrs.String(), gotErr.String())
	})
}

func TestBeforeDestroy(t *testing.T) {
	OpenDB("test")
	defer t.Cleanup(func() {
		dbConn.Conn.TruncateAll()
	})

	r := require.New(t)
	tax := Tax{
		Name:       "15% VAT",
		Percentage: 10,
	}
	err := dbConn.Conn.Create(&tax)
	r.NoError(err)

	cat := Category{
		Name: "Sweets",
	}
	err = dbConn.Conn.Create(&cat)
	r.NoError(err)

	product := Product{
		ID: 1, Name: "Grape", Price: 10,
		Sku: "PR001", SalePrice: nulls.Float64{10, true},
		Description: nulls.String{"Grape", true},
		CategoryID:  cat.ID, TaxID: tax.ID,
	}
	err = dbConn.Conn.Create(&product)
	r.NoError(err)

	t.Run("When product id is not referred in TransactionLineItem", func(t1 *testing.T) {
		err = product.BeforeDestroy(dbConn.Conn)
		r.NoError(err)
	})

	t.Run("When product is referred in TransactionLineItem", func(t1 *testing.T) {
		contact := Contact{Name: "Test Customer"}
		shop := Shop{Name: "Branch"}
		err := dbConn.Conn.Create(&contact)
		r.NoError(err)
		err = dbConn.Conn.Create(&shop)
		r.NoError(err)
		tDocument := TransactionDocument{
			Reference: "INV01", DocType: "Invoice",
			IssueDate: "2021-05-30", DueDate: "2021-05-30",
			ContactID: contact.ID, ShopID: shop.ID,
			TransactionLineItems: TransactionLineItems{
				TransactionLineItem{
					ProductID: product.ID, UnitPrice: nulls.Float64{10, true},
					Quantity: 1, DiscountPercentage: nulls.Float64{0, true},
				},
			},
		}
		err = dbConn.Conn.Create(&tDocument)
		r.NoError(err)
		line := tDocument.TransactionLineItems[0]
		line.TransactionDocumentID = tDocument.ID
		err = dbConn.Conn.Create(&line)
		r.NoError(err)

		err = product.BeforeDestroy(dbConn.Conn)
		r.Error(err)
	})
}

func TestProducts(t *testing.T) {
	OpenDB("test")
	defer t.Cleanup(func() {
		dbConn.Conn.TruncateAll()
	})

	r := require.New(t)

	tax := Tax{
		Name:       "15% VAT",
		Percentage: 10,
	}
	err := dbConn.Conn.Create(&tax)
	r.NoError(err)

	cat := Category{
		Name: "Sweets",
	}
	err = dbConn.Conn.Create(&cat)
	r.NoError(err)

	product := Product{
		Name: "Grape", Price: 10,
		Sku: "PR001", SalePrice: nulls.Float64{10, true},
		Description: nulls.String{"Grape", true},
		CategoryID:  cat.ID, TaxID: tax.ID,
	}
	err = dbConn.Conn.Create(&product)
	r.NoError(err)

	t.Run("when where query is passed", func(t1 *testing.T) {
		prods, err := dbConn.Products("price = 100")
		r.NoError(err)
		assert.Empty(t1, prods)

		prods, err = dbConn.Products("name = 'Grape'")
		r.NoError(err)
		assert.Equal(t1, prods[0].ID, product.ID)
		assert.Equal(t1, 1, len(prods))
	})

	t.Run("when query is empty", func(t1 *testing.T) {
		prods, err := dbConn.Products("")
		r.NoError(err)
		assert.Equal(t1, 1, len(prods))
	})

	t.Run("find product", func(t1 *testing.T) {
		prod, err := dbConn.Product(int(product.ID))
		r.NoError(err)
		assert.Equal(t1, prod.ID, product.ID)
		_, err = dbConn.Product(100)
		r.Error(err)
	})

	t.Run("Create product", func(t1 *testing.T) {
		t1.Run("When validation fails", func(t2 *testing.T) {
			prod := Product{
				Sku: product.Sku, Name: "Apple",
			}
			verrs, err := dbConn.CreateProduct(&prod)
			r.NoError(err)
			assert.Equal(t2, verrs.HasAny(), true)
		})

		t1.Run("when creation is successfull", func(t2 *testing.T) {
			prod := Product{
				Sku: "PR002", Name: "Apple",
				Price: 10, SalePrice: nulls.Float64{10, true},
				Description: nulls.String{"Grape", true},
				CategoryID:  cat.ID, TaxID: tax.ID,
			}
			verrs, err := dbConn.CreateProduct(&prod)
			r.NoError(err)
			assert.Equal(t2, verrs.HasAny(), false)
			if prod.ID == 0 {
				t2.Errorf("product %s not saved", prod.Sku)
			}
		})
	})

	t.Run("Update product", func(t1 *testing.T) {
		t1.Run("When validation fails", func(t2 *testing.T) {
			product.CategoryID = 0
			verrs, err := dbConn.UpdateProduct(&product)
			r.NoError(err)
			assert.Equal(t2, verrs.HasAny(), true)
		})

		t1.Run("when update is successfull", func(t2 *testing.T) {
			product.Sku = "PR007"
			product.CategoryID = cat.ID
			verrs, err := dbConn.UpdateProduct(&product)
			r.NoError(err)
			assert.Equal(t2, verrs.HasAny(), false)
		})
	})

	t.Run("Delete product", func(t1 *testing.T) {
		t1.Run("When product cannot be found", func(t2 *testing.T) {
			err := dbConn.DeleteProduct(587)
			r.Error(err)
		})

		t1.Run("when product with ID found", func(t2 *testing.T) {
			err := dbConn.DeleteProduct(product.ID)
			r.NoError(err)
		})
	})
}

func addWantedErrors(wErr *validate.Errors) {
	wErr.Add("category", "Category must be present")
	wErr.Add("sku", "Sku cannot be empty")
	wErr.Add("tax", "Tax must be present")
}
