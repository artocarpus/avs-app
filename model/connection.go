package model

import (
	"database/sql"
	"fmt"

	"github.com/fatih/color"
	"github.com/gobuffalo/logger"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/logging"
	_ "github.com/lib/pq" // postgres driver
)

//DB pointer to *sql.DB
type DB struct {
	Conn  *pop.Connection
	sqlDB *sql.DB
}

var dbConn DB

// OpenDB database connection
func OpenDB(env string) (*DB, error) {
	fmt.Printf("Connecting to env : %s\n", env)
	db, err := pop.Connect(env)
	if err != nil {
		return nil, err
	}
	sqlDB, _ := sql.Open("postgres", db.URL())
	dbConn = DB{Conn: db, sqlDB: sqlDB}
	if env != "test" {
		setLog()
	}
	return &dbConn, nil
}

func setLog() {
	pop.SetLogger(func(lvl logging.Level, s string, args ...interface{}) {
		l := logger.NewLogger("info")

		if lvl == logging.SQL {
			if len(args) > 0 {
				xargs := make([]string, len(args))
				for i, a := range args {
					switch a.(type) {
					case string:
						xargs[i] = fmt.Sprintf("%q", a)
					default:
						xargs[i] = fmt.Sprintf("%v", a)
					}
				}
				s = fmt.Sprintf("%s - %s | %s", lvl, s, xargs)
			} else {
				s = fmt.Sprintf("%s - %s", lvl, s)
			}
		} else {
			s = fmt.Sprintf(s, args...)
			s = fmt.Sprintf("%s - %s", lvl, s)
		}
		s = color.YellowString(s)
		l.Info(s)
	})
}
