This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Available scripts

## Build React

### Build for production
```bash
npm run build
```

### Build for development
```bash
npm run build:dev
```

### Watch server
```bash
npm run build:watch
```

### Webpack server

```bash
npm run start
```

# Server

```bash
go build
```

## Migrate

```bash
./avs-app --migrate
```

## Start server

### In development

```bash
./avs-app server --dev
```

### Port and binding options

```bash
./avs-app server --port=8000 --bind=0.0.0.0
```