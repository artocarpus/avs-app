package application

import (
	"fmt"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/harikrishnansr92/avs-app/model"
)

// AppClaims payload for jwt token
type AppClaims struct {
	User map[string]interface{} `json:"user"`
	jwt.StandardClaims
}

// Token represents JWT token, expire time and user id for which token is generated
type Token struct {
	Token     string `json:"token"`
	ExpiresAt int64  `json:"expires_at"`
	UserID    int64  `json:"user_id"`
}

// TokenData contains user
type TokenData struct {
	User *model.User
}

// CreateKey generates a jwt token
func CreateKey(user *model.User) (Token, error) {
	data := map[string]interface{}{
		"id":        int64(user.ID),
		"full_name": user.FullName.String,
		"email":     user.Email,
	}
	expireTime := time.Now().AddDate(0, 0, 2).Unix()
	claims := AppClaims{
		data,
		jwt.StandardClaims{
			Id:        fmt.Sprint(user.ID),
			ExpiresAt: expireTime,
			Issuer:    "avsapp-server",
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenData, err := token.SignedString(App.SigningKey)
	resp := Token{
		Token:     tokenData,
		ExpiresAt: claims.ExpiresAt,
		UserID:    user.ID,
	}
	return resp, err
}

// ParseKey parses jwt token
func ParseKey(tokenString string) (TokenData, error) {
	td := TokenData{
		User: &model.User{},
	}
	ts := getToken(tokenString)
	token, err := jwt.ParseWithClaims(ts, &AppClaims{}, func(token *jwt.Token) (interface{}, error) {
		return App.SigningKey, nil
	})

	if err != nil {
		return td, err
	}

	claims, ok := token.Claims.(*AppClaims)

	if ok && token.Valid {
		err = App.DB.Conn.Find(td.User, claims.User["id"])
	}
	return td, err
}

func getToken(bearerString string) string {
	ts := strings.Split(bearerString, "Bearer ")

	if len(ts) == 2 {
		return ts[1]
	}
	return ""
}
