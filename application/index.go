package application

import (
	"fmt"

	"gitlab.com/harikrishnansr92/avs-app/model"
)

// Config represents th configuration for each connection
type Config struct {
	DB          *model.DB
	User        *model.User
	SigningKey  []byte
	Host        string
	Port        int64
	Environment string
}

var (
	// App is variable for instance of Config
	App *Config
)

// IsUserSignedIn returns boolean indicating whether user is signed in or not
func (app *Config) IsUserSignedIn() bool {
	return !(app.User == nil || app.User.ID == 0)
}

// Authorize returns error if user is not signed in
func (app *Config) Authorize() error {
	if app.IsUserSignedIn() {
		return nil
	}

	return fmt.Errorf("Unauthorized")
}
