PROJECT_NAME := "avs-app"
PKG := "gitlab.com/harikrishnansr92/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
env=development
ifeq ($(DBURL),)
DBURL := postgres://postgres:postgres@localhost:5432/avs_app_dev?sslmode=disable
endif

.PHONY: all dep build clean test coverage coverhtml server run key

all: build

lint: ## Lint the files 
	${GOPATH}/bin/golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	make coverage
	@go tool cover -html=cover/coverage.cov -o coverage.html

dep: ## Get the dependencies
	@go get -v -d ./...

build: dep ## Build the binary file
	@go build -v $(PKG)

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

migrate_up: ## Migrate database with db URL
	migrate -path ./model/migrations -database $(DBURL) up
migrate_down: ## Rollback last change with DBURL
	migrate -path ./model/migrations -database $(DBURL) down
truncate_db: ## Truncate database with url=URL
	migrate -path ./model/migrations -database $(DBURL) drop

server: ## Run server (--port=3000 --bind=localhost)
	./$(PROJECT_NAME) server --port=8000 --bind=0.0.0.0 --env=$(env)

run: ## run server as script
	go run . server --port=8000 --bind=0.0.0.0

key: ## Generate random key
	./$(PROJECT_NAME) --key

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

