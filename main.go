package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
	"gitlab.com/harikrishnansr92/avs-app/server"
)

func main() {
	var db *model.DB
	var modelError error
	run := flag.NewFlagSet("server", flag.ExitOnError)
	port := run.Int64("port", 3000, "Port to run server")
	bind := run.String("bind", "", "bind address")
	serverEnv := run.String("env", "development", "Running development")

	genKey := flag.Bool("key", false, "Generate secret key")
	migrate := flag.NewFlagSet("migrate", flag.ExitOnError)
	up := migrate.Bool("up", false, "Run migration")
	down := migrate.Bool("down", false, "Run migration")
	drop := migrate.Bool("truncate", false, "Truncate DB")
	dbEnv := migrate.String("env", "development", "Running development")
	flag.Parse()

	for i, arg := range os.Args[1:] {
		switch arg {
		case "migrate":
			migrate.Parse(os.Args[(i + 2):])
			db, modelError = model.OpenDB(*dbEnv)
			if db == nil {
				log.Println("Can not connect to db : ", modelError.Error())
			}

			if *up == true {
				if err := db.MigrateUp(); err != nil {
					log.Printf("Can not run migration, %s \n", err.Error())
				}
			} else if *down == true {
				db.MigrateDown()
			} else if *drop == true {
				db.Truncate()
			} else {
				fmt.Println("Specify migration direction(--up or --down)")
			}
			return
		case "server":
			run.Parse(os.Args[(i + 2):])
			db, modelError = model.OpenDB(*serverEnv)
			if db == nil {
				log.Println("Can not connect to db : ", modelError.Error())
			}

			application.App = &application.Config{
				DB:          db,
				SigningKey:  []byte(os.Getenv("APP_KEY")),
				Host:        *bind,
				Port:        *port,
				Environment: *serverEnv,
			}
			server.Run()
		case "--key":
			if *genKey {
				fmt.Println(generateKey())
			}
		}
	}
}
