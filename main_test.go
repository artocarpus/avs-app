package main

import (
	"flag"
	"os"
	"testing"
)

func TestMain(t *testing.T) {
	cases := []struct {
		Name           string
		Args           []string
		ExpectedExit   int
		ExpectedOutput string
	}{
		{
			"server",
			[]string{
				"server", "--port=8080",
				"--bind=0.0.0.0", "--env=test",
			},
			0, "",
		},
	}
	for _, tc := range cases {
		flag.CommandLine = flag.NewFlagSet(tc.Name, flag.ExitOnError)
		os.Args = append([]string{tc.Name}, tc.Args...)
		main()
	}
}
