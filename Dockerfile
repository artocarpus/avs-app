FROM registry.gitlab.com/artocarpus/gowithclang:latest

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN apt-get install -y make; \
    go get -u golang.org/x/lint/golint;

COPY ./ ./

EXPOSE 8000

RUN make

CMD ["make run"]
