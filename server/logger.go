package server

import (
	"log"
	"net/http"
	"time"
)

// Logger handles logging for each handler
// pass the handler to this function
func Logger(inner http.Handler, name string) http.Handler {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		inner.ServeHTTP(w, r)
		log.Printf(
			"%s\t%s\t%s\t%s", r.Method, r.RequestURI, name, time.Since(start),
		)
	})
	return handler
}
