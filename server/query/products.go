package query

import (
	"fmt"

	"github.com/gobuffalo/pop"
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
	"gitlab.com/harikrishnansr92/avs-app/server/types"
)

// Products returns graphql query for products
var Products = &graphql.Field{
	Name: "products",
	Args: graphql.FieldConfigArgument{
		"product": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "ProductQuery",
				Fields: graphql.InputObjectConfigFieldMap{
					"name": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"sku": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"price_lt": &graphql.InputObjectFieldConfig{
						Type: graphql.Float,
					},
					"price_gt": &graphql.InputObjectFieldConfig{
						Type: graphql.Float,
					},
				},
			}),
		},
	},
	Type:        graphql.NewList(types.Product),
	Description: "Fetch all products",
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		if err := application.App.Authorize(); err != nil {
			return nil, err
		}

		products := []model.Product{}
		if err := buildProductQuery(params.Args["product"]).All(&products); err != nil {
			return nil, fmt.Errorf("Failed to fetch products, %s", err.Error())
		}

		return products, nil
	},
}

func buildProductQuery(params interface{}) *pop.Query {
	query := application.App.DB.Conn.Q()
	productParams, isOk := params.(map[string]interface{})
	if !isOk {
		return query
	}

	if productParams["name"] != nil {
		query.Where("products.name LIKE ?", productParams["name"])
	}
	return query
}
