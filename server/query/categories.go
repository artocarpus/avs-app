package query

import (
	"github.com/gobuffalo/pop"
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
	"gitlab.com/harikrishnansr92/avs-app/server/types"
)

// Categories to fetch all categories for current user
var Categories = &graphql.Field{
	Name: "Categories",
	Args: graphql.FieldConfigArgument{
		"category": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "CategoryQueryType",
				Fields: graphql.InputObjectConfigFieldMap{
					"name": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
				},
			}),
		},
	},
	Type:        graphql.NewList(types.Category),
	Description: "Get current users categories",
	Resolve:     fetchCategories,
}

func fetchCategories(params graphql.ResolveParams) (interface{}, error) {
	if err := application.App.Authorize(); err != nil {
		return nil, err
	}

	categoryParams, _ := params.Args["category"].(map[string]interface{})
	categories := []model.Category{}

	err := buildCategoryQuery(categoryParams).All(&categories)

	if err != nil {
		return nil, err
	}
	return categories, nil
}

func buildCategoryQuery(params map[string]interface{}) *pop.Query {
	q := application.App.DB.Conn.Q()
	if params["name"] != nil {
		q.Where("categories.name ILIKE ?", params["name"])
	}

	return q
}
