package query

import (
	"fmt"

	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
	"gitlab.com/harikrishnansr92/avs-app/server/types"
)

// Tax finds the tax with ID
var Tax = &graphql.Field{
	Name: "Tax",
	Args: graphql.FieldConfigArgument{
		"tax": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "TaxQueryType",
				Fields: graphql.InputObjectConfigFieldMap{
					"id": &graphql.InputObjectFieldConfig{
						Type: graphql.ID,
					},
				},
			}),
		},
	},
	Type:        types.Tax,
	Description: "Get current users taxes",
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		if err := application.App.Authorize(); err != nil {
			return nil, err
		}

		tax := model.Tax{}

		taxParams, isOk := params.Args["tax"].(map[string]interface{})
		if !isOk {
			return nil, fmt.Errorf("Invalid tax id %v", taxParams)
		}

		if err := application.App.DB.Conn.Find(&tax, taxParams["id"]); err != nil {
			return nil, fmt.Errorf("Error finding tax\n %s", err.Error())
		}
		return tax, nil
	},
}
