package query

import (
	"fmt"

	"github.com/gobuffalo/pop"
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
	"gitlab.com/harikrishnansr92/avs-app/server/types"
)

// Taxes to fetch all taxes for current user
var Taxes = &graphql.Field{
	Name: "Taxes",
	Args: graphql.FieldConfigArgument{
		"tax": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "TaxesQueryType",
				Fields: graphql.InputObjectConfigFieldMap{
					"name": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
				},
			}),
		},
	},
	Type:        graphql.NewList(types.Tax),
	Description: "Get current users taxes",
	Resolve:     fetchTaxes,
}

func fetchTaxes(params graphql.ResolveParams) (interface{}, error) {
	if err := application.App.Authorize(); err != nil {
		return nil, err
	}

	taxParams, _ := params.Args["tax"].(map[string]interface{})
	taxes := []model.Tax{}

	err := buildTaxQuery(taxParams).All(&taxes)

	if err != nil {
		return nil, err
	}
	fmt.Println(taxes)
	return taxes, nil
}

func buildTaxQuery(params map[string]interface{}) *pop.Query {
	q := application.App.DB.Conn.Q()
	if params["name"] != nil {
		q.Where("taxes.name ILIKE ?", params["name"])
	}

	return q
}
