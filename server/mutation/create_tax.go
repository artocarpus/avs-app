package mutation

import (
	"fmt"

	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
)

// CreateTax creates a tax
var CreateTax = &graphql.Field{
	Name: "CreateTax",
	Type: graphql.ID,
	Args: graphql.FieldConfigArgument{
		"tax": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "taxInput",
				Fields: graphql.InputObjectConfigFieldMap{
					"name": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"percentage": &graphql.InputObjectFieldConfig{
						Type: graphql.Float,
					},
				},
			}),
		},
	},
	Description: "Create a tax",
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		if err := application.App.Authorize(); err != nil {
			return nil, err
		}

		data, isOk := params.Args["tax"].(map[string]interface{})
		if !isOk {
			return nil, fmt.Errorf("Invalid arguments")
		}
		tax := model.Tax{
			Name:       data["name"].(string),
			Percentage: data["percentage"].(float64),
		}

		vErrors, err := application.App.DB.Conn.ValidateAndCreate(&tax)
		if err != nil {
			return nil, err
		}

		if vErrors.HasAny() {
			return nil, fmt.Errorf("Tax : %s", vErrors)
		}

		return tax.ID, nil
	},
}
