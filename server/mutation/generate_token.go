package mutation

import (
	"fmt"
	"time"

	"github.com/gobuffalo/pop/nulls"
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
	"gitlab.com/harikrishnansr92/avs-app/server/types"
)

// GenerateToken is graphql field mutation
var GenerateToken = &graphql.Field{
	Name: "generateToken",
	Type: types.Token,
	Args: graphql.FieldConfigArgument{
		"user": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "TokenInputType",
				Fields: graphql.InputObjectConfigFieldMap{
					"email": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"password": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
				},
			}),
		},
	},
	Description: "generate JWT token",
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		userData := params.Args["user"].(map[string]interface{})
		var err error
		user := &model.User{}

		user, err = signIn(userData)
		if err != nil {
			return nil, err
		}

		if err != nil {
			return nil, err
		}

		token, err := application.CreateKey(user)
		if err != nil {
			return nil, err
		}

		return token, nil
	},
}

func findUser(email string, password string) (*model.User, error) {
	user := model.User{}
	err := application.App.DB.UserByEmail(email, &user)
	if err != nil {
		return nil, fmt.Errorf("failed to find user with email %s", email)
	}

	err = user.ComparePasswords(password)
	if err != nil {
		passwordInvalid := fmt.Errorf("Invalid password")
		return nil, passwordInvalid
	}

	return &user, nil
}

func signIn(params map[string]interface{}) (*model.User, error) {
	email, isOk := params["email"].(string)
	if !isOk {
		return nil, fmt.Errorf("Invalid email")
	}
	password, isOk := params["password"].(string)
	if !isOk {
		return nil, fmt.Errorf("Invalid password")
	}

	user, err := findUser(email, password)
	if err != nil {
		return nil, err
	}

	user.LastSignInAt = user.CurrentSignInAt
	user.CurrentSignInAt = nulls.NewTime(time.Now())
	user.SignInCount = user.SignInCount + 1
	user.LastSignInIP = user.CurrentSignInIP
	err = application.App.DB.Conn.Update(user)

	return user, nil
}
