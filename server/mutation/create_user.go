package mutation

import (
	"fmt"

	"github.com/gobuffalo/nulls"
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
)

// CreateUser creates a new user
var CreateUser = &graphql.Field{
	Name: "createUser",
	Type: graphql.ID,
	Args: graphql.FieldConfigArgument{
		"user": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "userInputType",
				Fields: graphql.InputObjectConfigFieldMap{
					"name": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"email": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"password": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"confirm_password": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
				},
			}),
		},
	},
	Description: "Creates a new user",
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		userParams := params.Args["user"].(map[string]interface{})
		name, _ := userParams["name"].(string)
		email, _ := userParams["email"].(string)
		password, _ := userParams["password"].(string)
		confirm, _ := userParams["confirm_password"].(string)
		user := model.User{
			FullName:        nulls.NewString(name),
			Email:           email,
			Password:        password,
			ConfirmPassword: confirm,
		}
		vErrors, err := application.App.DB.CreateUser(&user)
		if err != nil {
			return nil, err
		}

		if vErrors.HasAny() {
			return nil, fmt.Errorf("User : %s", vErrors)
		}

		return user.ID, nil
	},
}
