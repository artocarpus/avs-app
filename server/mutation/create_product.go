package mutation

import (
	"fmt"

	"github.com/gobuffalo/nulls"
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
)

// CreateProduct is graphql mutation function to create a product
var CreateProduct = &graphql.Field{
	Name:        "CreateProduct",
	Description: "Create a product",
	Type:        graphql.ID,
	Args: graphql.FieldConfigArgument{
		"product": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "CreateProductInput",
				Fields: graphql.InputObjectConfigFieldMap{
					"name": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"sku": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"price": &graphql.InputObjectFieldConfig{
						Type: graphql.Float,
					},
					"sales_price": &graphql.InputObjectFieldConfig{
						Type: graphql.Float,
					},
					"description": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"category_id": &graphql.InputObjectFieldConfig{
						Type: graphql.Int,
					},
					"tax_id": &graphql.InputObjectFieldConfig{
						Type: graphql.Int,
					},
				},
			}),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		if err := application.App.Authorize(); err != nil {
			return nil, err
		}

		productParams, isOk := params.Args["product"].(map[string]interface{})
		if !isOk || productParams == nil {
			return nil, fmt.Errorf("Invalid parameters")
		}

		product := model.Product{}
		if v, ok := productParams["sku"].(string); ok {
			product.Sku = v
		}
		if v, ok := productParams["name"].(string); ok {
			product.Name = v
		}
		if v, ok := productParams["description"].(string); ok {
			product.Description = nulls.NewString(v)
		}
		if v, ok := productParams["price"].(float64); ok {
			product.Price = v
		}
		if v, ok := productParams["sales_price"].(float64); ok {
			product.SalePrice = nulls.NewFloat64(v)
		}
		if v, ok := productParams["category_id"].(int); ok {
			product.CategoryID = int64(v)
		}
		if v, ok := productParams["tax_id"].(int); ok {
			product.TaxID = int64(v)
		}

		verrors, err := application.App.DB.Conn.ValidateAndCreate(&product)
		if err != nil {
			return nil, err
		}
		if verrors.HasAny() {
			return nil, fmt.Errorf("Error creating product %s", verrors.String())
		}

		return product.ID, nil
	},
}
