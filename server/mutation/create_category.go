package mutation

import (
	"fmt"

	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/application"
	"gitlab.com/harikrishnansr92/avs-app/model"
)

// CreateCategory mutation endpoint to create a new category.
var CreateCategory = &graphql.Field{
	Name: "createCategory",
	Type: graphql.ID,
	Args: graphql.FieldConfigArgument{
		"category": &graphql.ArgumentConfig{
			Type: graphql.NewInputObject(graphql.InputObjectConfig{
				Name: "CategoryInputType",
				Fields: graphql.InputObjectConfigFieldMap{
					"name": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
				},
			}),
		},
	},
	Description: "Create Category",
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		if err := application.App.Authorize(); err != nil {
			return nil, err
		}

		data := params.Args["category"].(map[string]interface{})
		categoryName, isOk := data["name"].(string)
		if !isOk {
			return nil, fmt.Errorf("Invalid category name")
		}
		category := model.Category{
			Name: categoryName,
		}

		vErrors, err := application.App.DB.Conn.ValidateAndCreate(&category)
		if err != nil {
			return nil, err
		}

		if vErrors.HasAny() {
			return nil, fmt.Errorf("Category : %s", vErrors)
		}

		return category.ID, nil
	},
}
