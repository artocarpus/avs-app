package types

import (
	"github.com/graphql-go/graphql"
)

// Token type
var Token = graphql.NewObject(graphql.ObjectConfig{
	Name: "TokenType",
	Fields: graphql.Fields{
		"token": &graphql.Field{
			Type: graphql.String,
		},
		"expires_at": &graphql.Field{
			Type: graphql.Int,
		},
		"user_id": &graphql.Field{
			Type: graphql.Int,
		},
	},
})
