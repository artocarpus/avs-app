package types

import (
	"github.com/graphql-go/graphql"
)

// Tax type
var Tax = graphql.NewObject(graphql.ObjectConfig{
	Name: "Tax",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"percentage": &graphql.Field{
			Type: graphql.Float,
		},
		"created_at": &graphql.Field{
			Type: graphql.DateTime,
		},
		"updated_at": &graphql.Field{
			Type: graphql.DateTime,
		},
	},
})
