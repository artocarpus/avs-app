package server

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/server/query"
)

func rootQuery() *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Name: "RootQuery",
		Fields: graphql.Fields{
			"categories": query.Categories,
			"taxes":      query.Taxes,
			"tax":        query.Tax,
			"products":   query.Products,
		},
	})
}
