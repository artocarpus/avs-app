package server

import (
	"fmt"
	"net/http"

	"gitlab.com/harikrishnansr92/avs-app/application"
)

// Authorize to compare the secret
func Authorize(inner http.Handler) http.Handler {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")
		td, err := application.ParseKey(auth)
		if err != nil {
			fmt.Println("Error parsing token")
		}
		application.App.User = td.User

		inner.ServeHTTP(w, r)
	})
	return handler
}
