package server

import (
	"fmt"
	"net/http"

	"gitlab.com/harikrishnansr92/avs-app/application"
)

// Run the server on bind adderess and port given
func Run() {
	serverDetails := fmt.Sprintf("%s:%d", application.App.Host, application.App.Port)
	handler := Authorize(GraphQLServer())

	if application.App.Environment == "test" {
		return
	}

	http.Handle("/graphql", handler)
	http.ListenAndServe(serverDetails, handler)
}
