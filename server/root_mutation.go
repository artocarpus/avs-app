package server

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/harikrishnansr92/avs-app/server/mutation"
)

func rootMutation() *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Name: "RootMutation",
		Fields: graphql.Fields{
			"generateToken":  mutation.GenerateToken,
			"createUser":     mutation.CreateUser,
			"createCategory": mutation.CreateCategory,
			"createTax":      mutation.CreateTax,
			"createProduct":  mutation.CreateProduct,
		},
	})
}
